<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->exchange_declare('logs', 'fanout', false, false, false);

//https://blog.csdn.net/why444216978/article/details/105881131
//消息有效期
//$m_table = new \PhpAmqpLib\Wire\AMQPTable();
//$m_table->set('x-message-ttl', 60000 );
//
//$channel->queue_declare('normalQueue',
//    false, true, false, false, false, $m_table);


//$m_table = new \PhpAmqpLib\Wire\AMQPTable();
//$m_table->set('x-expires', 60000 );
//$channel->queue_declare('normalQueue',
//    false, true, false, false, false, $m_table);
//$channel->queue_bind('normalQueue', 'normalExchange', 'normalKey');


$data = implode(' ', array_slice($argv, 1));
if (empty($data)) {
    $data = "info: Hello World!";
}
$msg = new AMQPMessage($data);

$channel->basic_publish($msg, 'logs');

echo ' [x] Sent ', $data, "\n";

$channel->close();
$connection->close();
?>
