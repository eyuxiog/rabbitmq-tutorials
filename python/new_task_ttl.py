#!/usr/bin/env python
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost', heartbeat=0))
# aaa = connection.process_data_events()
# print(aaa)
channel = connection.channel()

exchangeName = 'exchange.normal'
exchangeDlxName = 'exchange.dlx'
queueName = 'queue.normal'
queueDlxName = 'queue.dlx'
routing_key = 'routingkey'

channel.exchange_declare(exchange=exchangeDlxName, exchange_type='direct', passive=False, durable=True)
channel.exchange_declare(exchange=exchangeName, exchange_type='fanout', passive=False, durable=True)

channel.queue_declare(queue=queueName, passive=False, durable=True, exclusive=False, auto_delete=False, arguments={
    # 死信队列
    'x-message-ttl': 16000,
    'x-dead-letter-exchange': exchangeDlxName,
    'x-dead-letter-routing-key': routing_key,
})
channel.queue_declare(queue=queueDlxName, passive=False, durable=True, exclusive=False, auto_delete=False)

channel.queue_bind(queue=queueName, exchange=exchangeName)
channel.queue_bind(queue=queueDlxName, exchange=exchangeDlxName, routing_key=routing_key)
# DLX = 'RetryExchange'
# routing_key = 'routing_key'
# channel.queue_declare(queue='task_queue1', arguments={
#     # 队列优先级，数字越高则优先级越高 1-10
#     'x-max-priority': 10,
#     # 队列过期时间
#     'x-expires': 6000,
#     'x-message-ttl': 6000,
#     # 死信队列
#     'x-dead-letter-exchange':DLX,
#     'x-dead-letter-routing-key':routing_key,
# })

# exchange_type = "direct"
# channel.exchange_declare(exchange="direct_logs", exchange_type=exchange_type, durable=True)
stocks = ['688668.XSHG', '688669.XSHG', '688678.XSHG', '688679.XSHG', '688680.XSHG', '688686.XSHG', '688689.XSHG',
          '688698.XSHG', '688699.XSHG', '688777.XSHG', '688788.XSHG', '688819.XSHG', '688981.XSHG', '689009.XSHG']
for i in range(20):
    message = ' '.join(sys.argv[1:]) or "Hello World!" + str(i)
    # message = stocks[i]
    channel.basic_publish(
        exchange=exchangeName,
        routing_key='rk',
        body=message,
        properties=pika.BasicProperties(
            # expiration=str(i)+'0000',  #消息过期时间
            # expiration='10000',  # 消息过期时间
            # priority=i,  # 消息优先级 1-10
            # delivery_mode=2,  # make message persistent
        )
    )
    print(" [x] Sent %r" % message)
channel.close()
connection.close()


