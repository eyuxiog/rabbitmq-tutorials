#!/usr/bin/env python
import pika
from jqdatasdk import *

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')

auth('18928692919', 'Qq123456')

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)

def jqData():
    jq_price_day_fields = ['open', 'close', 'low', 'high', 'volume', 'money', 'high_limit', 'low_limit', 'avg','pre_close']
    df = get_price('000001.XSHE', start_date='2021-01-25', end_date='2021-01-25 23:00:00', frequency='1d', fields=jq_price_day_fields)
    print(df)
    return df


def on_request(ch, method, props, body):
    n = int(body)

    # print(" [.] fib(%s)" % n)
    jqData()
    response = fib(n)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
channel.start_consuming()
